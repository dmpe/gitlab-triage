# frozen_string_literal: true

require 'spec_helper'

require 'gitlab/triage/action'
require 'gitlab/triage/policies/rule_policy'

require 'active_support/core_ext/hash/except'
require 'active_support/core_ext/hash/indifferent_access'

describe Gitlab::Triage::Action do
  include_context 'with network context'

  let(:policy) do
    Gitlab::Triage::Policies::RulePolicy.new(
      'issues',
      { name: 'Test rule name', actions: actions_hash },
      [
        { title: 'Issue #0', web_url: 'http://example.com/0' },
        { title: 'Issue #1', web_url: 'http://example.com/1' }
      ],
      network
    )
  end

  let(:action_args) do
    {
      policy: policy,
      network: network
    }
  end

  let(:options) do
    {
      policy: policy,
      network: network,
      dry: dry
    }
  end

  let(:actions_hash) { rules }

  shared_examples 'acting with the right action' do |klass|
    context 'when doing actual run' do
      let(:dry) { false }

      it "uses #{klass} to process it" do
        expect_instance_call(klass, action_args, :act) do
          subject.process(**options)
        end
      end
    end

    context 'when doing dry-run' do
      let(:dry) { true }

      it "uses #{klass::Dry} to process it" do
        expect_instance_call(klass::Dry, action_args, :act) do
          subject.process(**options)
        end
      end
    end
  end

  describe '.process' do
    context 'when rules contain :summarize' do
      let(:actions_hash) { { summarize: { title: 'title' } } }

      it_behaves_like 'acting with the right action',
        described_class::Summarize
    end

    context 'when rules contain :issue' do
      let(:actions_hash) { { issue: { title: 'title' } } }

      it_behaves_like 'acting with the right action',
        described_class::Issue
    end

    context 'when rules contain :comment_on_summary' do
      let(:actions_hash) { { summarize: { title: 'title' }, comment_on_summary: 'foo' } }

      before do
        policy.summary = {
          project_id: 'dummy-project',
          iid: 'some-iid',
          web_url: 'some-url'
        }.with_indifferent_access

        s = instance_double(described_class::Summarize, act: nil)
        allow(described_class::Summarize).to receive(:new).and_return(s)
      end

      it_behaves_like 'acting with the right action',
        described_class::CommentOnSummary
    end

    context 'when rules contain :mention' do
      let(:actions_hash) { { mention: ['user'] } }

      it_behaves_like 'acting with the right action',
        described_class::Comment
    end

    context 'when rules contain :delete' do
      let(:actions_hash) { { delete: true } }

      it_behaves_like 'acting with the right action',
        described_class::Delete
    end

    context 'when rules contain :summarize and :mention' do
      let(:actions_hash) do
        {
          mention: ['user'],
          summarize: { title: 'title' }
        }
      end

      context 'when doing actual run' do
        let(:dry) { false }

        it "uses Summarize and Comment to process it" do
          s = instance_double(described_class::Summarize, act: nil)
          allow(described_class::Summarize).to receive(:new).with(action_args).and_return(s)

          c = instance_double(described_class::Comment, act: nil)
          allow(described_class::Comment).to receive(:new).with(action_args).and_return(c)

          subject.process(**options)

          expect(s).to have_received(:act)
          expect(c).to have_received(:act)
        end
      end

      context 'when doing dry-run' do
        let(:dry) { true }

        it "uses Summarize::Dry and Comment::Dry to process it" do
          expect_instance_call(described_class::Summarize::Dry, action_args, :act) do
            expect_instance_call(described_class::Comment::Dry, action_args, :act) do
              subject.process(**options)
            end
          end
        end
      end
    end
  end

  def expect_instance_call(klass, action_args, method)
    instance = instance_double(klass, method => nil)
    allow(klass).to receive(:new).with(action_args).and_return(instance)

    yield  # This runs the code block where the instance is likely to be used

    expect(instance).to have_received(method)
  end
end
