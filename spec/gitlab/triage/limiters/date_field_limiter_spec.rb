# frozen_string_literal: true

require 'spec_helper'

require 'gitlab/triage/limiters/date_field_limiter'

describe Gitlab::Triage::Limiters::DateFieldLimiter do
  let(:resources) do
    [
      { iid: 1, created_at: "2000-01-01T15:01:00.000Z" },
      { iid: 7, created_at: "2000-01-01T15:21:00.000Z" },
      { iid: 2, created_at: "2000-01-01T15:11:00.000Z" }
    ]
  end

  let(:threshold) { 2 }

  describe 'validation' do
    it 'throws an exception if conditions do not contain a valid limit type' do
      expect do
        described_class.new(resources, { invalid: threshold }).limit
      end.to raise_error(ArgumentError)
    end

    it 'throws an exception if conditions contain multiple limit types' do
      expect do
        described_class.new(resources, { most_recent: 2, oldest: 2 }).limit
      end.to raise_error(ArgumentError)
    end

    it 'throws an exception if threshold is not an integer' do
      expect do
        described_class.new(resources, { most_recent: "two" }).limit
      end.to raise_error(ArgumentError)
    end
  end

  subject { described_class.new(resources, condition) }

  describe '#limit' do
    describe 'most_recent' do
      let(:condition) do
        {
          most_recent: threshold
        }
      end

      it 'returns resources in descendant' do
        expected = [
          { iid: 7, created_at: "2000-01-01T15:21:00.000Z" },
          { iid: 2, created_at: "2000-01-01T15:11:00.000Z" }
        ]
        expect(subject.limit).to eq(expected)
      end
    end

    describe 'oldest' do
      let(:condition) do
        {
          oldest: threshold
        }
      end

      it 'returns resources in ascendant' do
        expected = [
          { iid: 1, created_at: "2000-01-01T15:01:00.000Z" },
          { iid: 2, created_at: "2000-01-01T15:11:00.000Z" }
        ]
        expect(subject.limit).to eq(expected)
      end
    end
  end
end
