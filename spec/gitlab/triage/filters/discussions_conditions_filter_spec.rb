# frozen_string_literal: true

require 'spec_helper'

require 'gitlab/triage/filters/discussions_conditions_filter'

describe Gitlab::Triage::Filters::DiscussionsConditionsFilter do
  let(:threads_count) { 3 }
  let(:user_notes_count) { 5 }

  let(:resource) do
    {
      user_notes_count: user_notes_count,
      user_discussions_count: threads_count
    }
  end

  let(:condition) do
    {
      attribute: 'notes',
      condition: 'greater_than',
      threshold: 3
    }
  end

  subject { described_class.new(resource, condition) }

  it_behaves_like 'a filter'

  describe '#resource_value' do
    it 'has the correct value for the notes attribute' do
      expect(subject.resource_value).to eq(user_notes_count)
    end

    it 'has the correct value for the threads attribute' do
      filter = described_class.new(resource, condition.merge(attribute: 'threads'))
      expect(filter.resource_value).to eq(threads_count)
    end
  end

  describe '#condition_value' do
    it 'has the correct value for comparison' do
      expect(subject.condition_value).to eq(3)
    end
  end

  describe '#calculate' do
    it 'calculates true given correct condition' do
      expect(subject.calculate).to be(true)
    end

    it 'calculate false given wrong condition' do
      filter = described_class.new(resource, condition.merge(condition: 'less_than'))
      expect(filter.calculate).to be(false)
    end
  end
end
